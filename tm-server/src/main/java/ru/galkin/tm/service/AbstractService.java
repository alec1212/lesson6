package ru.galkin.tm.service;

import ru.galkin.tm.api.ConnectionProvaider;

public abstract class AbstractService {

    protected final ConnectionProvaider connectionProvaider;

    protected AbstractService(ConnectionProvaider connectionProvaider) {
        this.connectionProvaider = connectionProvaider;
    }
}
