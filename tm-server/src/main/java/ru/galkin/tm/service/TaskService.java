package ru.galkin.tm.service;

import ru.galkin.tm.api.ConnectionProvaider;
import ru.galkin.tm.entity.Project;
import ru.galkin.tm.repository.Impl.TaskRepository;
import ru.galkin.tm.entity.Task;

import java.sql.SQLException;
import java.util.List;

public class TaskService extends AbstractService{

    private final TaskRepository taskRepository;

    protected TaskService(ConnectionProvaider connectionProvaider) throws SQLException {
        super(connectionProvaider);

        taskRepository = new TaskRepository(connectionProvaider.getConnection());
    }


    public Task create(String name) {
        return taskRepository.create(name);
    }



    public Task findByName(String name) {
        return taskRepository.findByName(name);
    }

    public Task removeById(String id) {
        return taskRepository.removeById(id);
    }

    public Task removeByName(String name) {
        return taskRepository.removeByName(name);
    }

    public Task findById(String id) {
        return taskRepository.findById(id);
    }

    public void clear() {
        taskRepository.clear();
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public Task update(String id, String name, String description)  {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return taskRepository.update(id, name, description);
    }

}
