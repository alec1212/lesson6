package ru.galkin.tm.service;

import ru.galkin.tm.api.ConnectionProvaider;
import ru.galkin.tm.endpoint.ProjectEndpoint;
import ru.galkin.tm.endpoint.TaskEndpoint;
import ru.galkin.tm.command.AbstractCommand;

import javax.xml.ws.Endpoint;
import java.sql.SQLException;
import java.util.*;

public final class Bootstrap {

    ConnectionProvaider connectionProvaider;

    private final Scanner terminalService = new Scanner(System.in);

//    private final TaskRepository taskRepository = new TaskRepository();
//
//    private final ProjectRepository projectRepository = new ProjectRepository();

    private final ProjectService projectService;

    private final TaskService taskService;


    private final ProjectEndpoint projectEndpoint;

    private final TaskEndpoint taskEndpoint;


    public ProjectService getProjectService() { return projectService;}

    public TaskService getTaskService() { return taskService;}


    public Scanner getTerminalService() {return terminalService;}


    public Bootstrap(ConnectionProvaider connectionProvaider) throws SQLException {
        this.connectionProvaider = connectionProvaider;

        projectService = new ProjectService(connectionProvaider);
        projectEndpoint = new ProjectEndpoint(projectService);

        taskService = new TaskService(connectionProvaider);
        taskEndpoint = new TaskEndpoint(taskService);

    }

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    public void registry(final AbstractCommand command) {
        final String cliCommand = command.command();
        final String cliDescription = command.description();
        if (cliCommand == null || cliCommand.isEmpty()) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (cliDescription == null || cliDescription.isEmpty()) try {
            throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }
    public void start() throws Exception {

        Endpoint.publish("http://0.0.0.0:8080/TaskEndpoint?wsdl", this.taskEndpoint);
        Endpoint.publish("http://0.0.0.0:8080/ProjectEndpoint?wsdl", this.projectEndpoint);


        final Scanner terminalService = new Scanner(System.in);
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        while (!"exit".equals(command)) {
            command = terminalService.nextLine();
            execute(command);
        }
    }
    private void execute(final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) {
            System.out.println("Unknown command");
            return;
        }
        abstractCommand.execute();
    }
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }
}