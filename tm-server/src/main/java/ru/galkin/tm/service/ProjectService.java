package ru.galkin.tm.service;

import ru.galkin.tm.api.ConnectionProvaider;
import ru.galkin.tm.repository.Impl.ProjectRepository;
import ru.galkin.tm.entity.Project;

import java.sql.SQLException;
import java.util.List;

public class ProjectService extends AbstractService{

    private final ProjectRepository projectRepository;

    protected ProjectService(ConnectionProvaider connectionProvaider) throws SQLException {
        super(connectionProvaider);
        projectRepository = new ProjectRepository(connectionProvaider.getConnection());
    }






    public Project create(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.create(name);
    }

    public Project create(String name, String description)  {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.create(name, description);
    }

    public Project update(String id, String name, String description)  {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return projectRepository.update(id, name, description);
    }

    public void clear()  {
        projectRepository.clear();
    }


    public Project findByName(String name)  {
        return projectRepository.findByName(name);
    }

    public Project findById(String id)  {
        return projectRepository.findById(id);
    }

    public Project removeById(String id)  {
        return projectRepository.removeById(id);
    }

    public Project removeByName(String name)  {
        return projectRepository.removeByName(name);
    }

    public List<Project> findAll()  {
        return projectRepository.findAll();
    }



}
