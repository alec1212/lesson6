package ru.galkin.tm.api;

import java.sql.Connection;
import java.sql.SQLException;

public interface ConnectionProvaider {

    Connection getConnection() throws SQLException;

}
