package ru.galkin.tm;

import org.reflections.Reflections;
import ru.galkin.tm.api.ConnectionProvaider;
import ru.galkin.tm.command.AbstractCommand;
import ru.galkin.tm.service.Bootstrap;
import ru.galkin.tm.service.PropertyService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Set;

/**
 * Тестовое приложение
 */
public class server {

    private static final PropertyService propertyService = new PropertyService();

    private static final ConnectionProvaider connectionProvaider = new ConnectionProvaider() {
        @Override
        public Connection getConnection()  {
            try {
                String host = propertyService.getHost();
                String port = propertyService.getPort();
                String database = propertyService.getDatabase();
                String username = propertyService.getUsername();
                String password = propertyService.getPassword();
                String jdbcString = String.format("jdbc:mysql://%s:%s/%s",host, port, database);
                return DriverManager.getConnection(jdbcString, username, password);
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                return null;

            }
        }
    };

    public static void main(final String[] args) throws Exception {

        Bootstrap bootstrap = new Bootstrap(connectionProvaider);

        Reflections reflections = new Reflections("ru.galkin.tm");
        Set<Class<? extends AbstractCommand>> commandClasses = reflections.getSubTypesOf(AbstractCommand.class);


        for (Class cmdClass : commandClasses) {
            if (AbstractCommand.class.isAssignableFrom(cmdClass)) {
                AbstractCommand cmd = (AbstractCommand) cmdClass.newInstance();
                bootstrap.registry(cmd);
            }
        }

        bootstrap.start();



    }


}




