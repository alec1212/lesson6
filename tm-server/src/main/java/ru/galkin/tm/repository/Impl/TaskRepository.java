package ru.galkin.tm.repository.Impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.uuid.Generators;
import ru.galkin.tm.entity.Task;
import ru.galkin.tm.repository.AbstarctRepository;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class TaskRepository extends AbstarctRepository {
    @XmlElement
    private List<Task> tasks = new ArrayList<>();

    public TaskRepository(Connection connection) {
        super(connection);
    }

    public Task create(final String name) {
        final Task project = new Task(name);
        this.create(project.getName(), project.getDescription());
        return project;
    }

    public Task create(final String name, final String description) {
        Task task = null;

        final String query = "INSERT INTO app_task (id, name, description) VALUES (?, ?, ?)";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            String uid = Generators.randomBasedGenerator().generate().toString();
            statement.setString(1, uid);
            statement.setString(2, name);
            statement.setString(3, description);
            statement.execute();

            task = new Task();
            task.setId(uid);
            task.setName(name);
            task.setDescription(description);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return task;

    }

    public Task update(final String id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        Task project = null;

        final String query = "UPDATE app_task SET name = ?,  description = ? WHERE id = ?";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, id);
            statement.execute();
            project = new Task();
            project.setId(id);
            project.setName(name);
            project.setDescription(description);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;

    }

    public void clear() {

        final String query = "DELETE FROM app_task ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Task findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        Task project = null;
        try {
            final String query = "SELECT id, name, description FROM app_task WHERE name = ? ";
            final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);

            final ResultSet resultSet;
            resultSet = statement.executeQuery();

            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project=  fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    public Task findById(final String id) {
        if (id == null) return null;
        Task project = null;

        final String query = "SELECT id, name, description FROM app_task WHERE id = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, id);
            final ResultSet resultSet = statement.executeQuery();
            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project =  fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }


    public Task removeById(final String id) {
        if (id == null) return null;
        Task project = null;

        final String query = "DELETE FROM  app_task WHERE id = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, id);
            final ResultSet resultSet = statement.executeQuery();
            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project =  fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    public Task removeByName(final String name)  {
        if (name == null || name.isEmpty()) return null;
        Task project = null;
        final String query = "DELETE FROM  app_task WHERE name = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, name);
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

//    public void load(List<Task> tasks) {
//
//        this.projects.clear();
//        this.projects = projects;
//    }

    @JsonProperty("Tasks")
    public List<Task> findAll() {
        final List<Task> tasks = new ArrayList<Task>();
        final String query = "SELECT id, name, description FROM app_task ";
        final Statement statement;
        try {
            statement = connection.createStatement();

            final ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                tasks.add(fetch(resultSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return tasks;
    }

    private Task fetch(final ResultSet resultSet) {
        if (resultSet == null) return null;
        final Task project = new Task();
        try {
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));

            project.setDescription(resultSet.getString("description"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;

    }


}
