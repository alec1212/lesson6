package ru.galkin.tm.repository.Impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.uuid.Generators;
import ru.galkin.tm.entity.Project;
import ru.galkin.tm.repository.AbstarctRepository;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class ProjectRepository extends AbstarctRepository {

    public ProjectRepository(Connection connection) {
        super(connection);
    }

    public Project create(final String name) {
        final Project project = new Project(name);
        this.create(project.getName(), project.getDescription());
        return project;
    }

    public Project create(final String name, final String description) {
        Project project = null;

        final String query = "INSERT INTO app_project (id, name, description) VALUES (?, ?, ?)";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            String uid = Generators.randomBasedGenerator().generate().toString();
            statement.setString(1, uid);
            statement.setString(2, name);
            statement.setString(3, description);
            statement.execute();

            project = new Project();
            project.setId(uid);
            project.setName(name);
            project.setDescription(description);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;

    }

    public Project update(final String id, final String name, final String description) {
        if (id == null) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = null;

        final String query = "UPDATE app_project SET name = ?,  description = ? WHERE id = ?";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, name);
            statement.setString(2, description);
            statement.setString(3, id);
            statement.execute();
            project = new Project();
            project.setId(id);
            project.setName(name);
            project.setDescription(description);

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;

    }

    public void clear() {

        final String query = "DELETE FROM app_project ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.execute();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = null;
        try {
            final String query = "SELECT id, name, description FROM app_project WHERE name = ? ";
            final PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, name);

            final ResultSet resultSet;
            resultSet = statement.executeQuery();

            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project = fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    public Project findById(final String id) {
        if (id == null) return null;
        Project project = null;

        final String query = "SELECT id, name, description FROM app_project WHERE id = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, id);
            final ResultSet resultSet = statement.executeQuery();
            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project = fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }


    public Project removeById(final String id) {
        if (id == null) return null;
        Project project = null;

        final String query = "DELETE FROM  app_project WHERE id = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, id);
            final ResultSet resultSet = statement.executeQuery();
            final Boolean hasNext = resultSet.next();
            if (!hasNext)
                return null;

            project = fetch(resultSet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;
    }

    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        Project project = null;
        final String query = "DELETE FROM  app_project WHERE name = ? ";
        final PreparedStatement statement;
        try {
            statement = connection.prepareStatement(query);

            statement.setString(1, name);
            statement.execute();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;

    }

//    public void load(List<Project> projects) {
//
//        this.projects.clear();
//        this.projects = projects;
//    }

    @JsonProperty("Projects")
    public List<Project> findAll() {
        final List<Project> projects = new ArrayList<Project>();
        final String query = "SELECT id, name, description FROM app_project ";
        final Statement statement;
        try {
            statement = connection.createStatement();

            final ResultSet resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                projects.add(fetch(resultSet));
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return projects;
    }

    private Project fetch(final ResultSet resultSet) {
        if (resultSet == null) return null;
        final Project project = new Project();
        try {
            project.setId(resultSet.getString("id"));
            project.setName(resultSet.getString("name"));

            project.setDescription(resultSet.getString("description"));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return project;

    }


}
