package ru.galkin.tm.command.ConcreateCommand.ProjectCommands;

import ru.galkin.tm.command.AbstractCommand;
import ru.galkin.tm.entity.Project;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() {

        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("PLEASE, ENTER PROJECT ID:");
        final String id = serviceLocator.getTerminalService().next();
        final Project project = serviceLocator.getProjectService().removeById(id);
        if (project == null)
            System.out.println("[FAIL]");
        else
            System.out.println("[OK]");

    }
}
