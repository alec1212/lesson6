package ru.galkin.tm.command.ConcreateCommand.ProjectCommands;

import ru.galkin.tm.command.AbstractCommand;
import ru.galkin.tm.entity.Project;

public final class ProjectUpdateByIndexCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-update-by-index";
    }

    @Override
    public String description() {
        return "Update project by index.";
    }

    @Override
    public void execute() {

        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER, PROJECT INDEX:");
        final String name = serviceLocator.getTerminalService().next();
        final Project project = serviceLocator.getProjectService().findByName(name);
        if (project == null) {
            System.out.println("FAIL]");
        }
        else {
            System.out.println("PLEASE, ENTER PROJECT NAME:");
            final String nameProject = serviceLocator.getTerminalService().nextLine();
            System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
            final String description = serviceLocator.getTerminalService().nextLine();
            serviceLocator.getProjectService().update(project.getId(), nameProject, description);
            System.out.println("[OK]");
        }

    }
}
