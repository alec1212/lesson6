package ru.galkin.tm.command.ConcreateCommand.TaskCommands;

import ru.galkin.tm.command.AbstractCommand;
import ru.galkin.tm.entity.Task;

public final class TaskViewCommand extends AbstractCommand {
    @Override
    public String command() {
        return "task-view";
    }

    @Override
    public String description() {
        return "View task.";
    }

    @Override
    public void execute() {

        System.out.println("ENTER, TASK ID:");
        final String id = serviceLocator.getTerminalService().next();
        final Task task = serviceLocator.getTaskService().findById(id);
        if (task == null)
            return;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: "+ task.getId());
        System.out.println("NAME: "+ task.getName());
        System.out.println("DESCRIPTION: "+ task.getDescription());
        System.out.println("[OK]");

    }
}
