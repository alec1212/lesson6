package ru.galkin.tm.command.ConcreateCommand.SystemCommands;

import ru.galkin.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {
    @Override
    public String command() {
        return "exit";
    }

    @Override
    public String description() {
        return "Exit application";
    }

    @Override
    public void execute() {

        System.exit(0);
        }
    }

