package ru.galkin.tm.command.ConcreateCommand.ProjectCommands;

import ru.galkin.tm.command.AbstractCommand;

import java.sql.SQLException;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create a new project.";
    }

    @Override
    public void execute() throws SQLException {

        System.out.println("[CREATE PROJECT]");
        System.out.println("PLEASE, ENTER PROJECT NAME:");
        final String name = serviceLocator.getTerminalService().nextLine();
        System.out.println("PLEASE, ENTER PROJECT DESCRIPTION:");
        final String description = serviceLocator.getTerminalService().nextLine();
        serviceLocator.getProjectService().create(name, description);
        System.out.println("[OK]");


    }
}
