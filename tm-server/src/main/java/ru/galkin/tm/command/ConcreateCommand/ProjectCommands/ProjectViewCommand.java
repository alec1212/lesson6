package ru.galkin.tm.command.ConcreateCommand.ProjectCommands;

import ru.galkin.tm.command.AbstractCommand;
import ru.galkin.tm.entity.Project;

public final class ProjectViewCommand extends AbstractCommand {

    @Override
    public String command() {
        return "project-view";
    }

    @Override
    public String description() {
        return "View project.";
    }

    @Override
    public void execute() {

        System.out.println("ENTER, PROJECT NAME:");
        final String name = serviceLocator.getTerminalService().next();
        final Project project = serviceLocator.getProjectService().findByName(name);
        if (project == null) return;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: "+ project.getId());
        System.out.println("NAME: "+ project.getName());
        System.out.println("DESCRIPTION: "+ project.getDescription());
        System.out.println("[OK]");

    }
}
