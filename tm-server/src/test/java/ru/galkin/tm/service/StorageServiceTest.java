/*
package ru.galkin.tm.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.galkin.tm.marker.IDataStorageTests;
import ru.galkin.tm.repository.Impl.ProjectRepository;
import ru.galkin.tm.repository.Impl.TaskRepository;

import java.io.File;

public class StorageServiceTest {

    private final ProjectRepository projectRepository =  new ProjectRepository();
    private final TaskRepository taskRepository =  new TaskRepository();
    private final StorageService storageService = new StorageService(projectRepository, taskRepository);

    @Before
    @Category(IDataStorageTests.class)
    public  void onStart()
    {
        File fileStorage = new File("data/xmlStorage.dat");
        if(fileStorage.exists()){

            fileStorage.delete();
            System.out.println("Файл data/xmlStorage.dat удалён");
        }

        taskRepository.create("TestTask");
        projectRepository.create("TestProject");
    }

    @Test
    @Category(IDataStorageTests.class)
    public void saveToXmlTest()
    {
        storageService.saveToXml();

        File fileStorage = new File("data/xmlStorage.dat");
        Assert.assertNotNull(fileStorage);
    }

    @Test
    @Category(IDataStorageTests.class)
    public void loadFromXmlTest()
    {
        storageService.saveToXml();
        projectRepository.clear();
        taskRepository.clear();
        storageService.loadFromXml();

        Assert.assertEquals(1, projectRepository.findAll().size());
        Assert.assertEquals(1, taskRepository.findAll().size());
        Assert.assertNotNull( projectRepository.findByName("TestProject"));
        Assert.assertNotNull( taskRepository.findByName("TestTask"));

    }
}
*/
